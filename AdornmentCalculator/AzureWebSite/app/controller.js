﻿( function () {
    'use strict';

    var controllerId = 'controller';

    angular.module( 'app' ).controller( controllerId,
        ['$scope', '$localStorage', 'datacontext', 'calculator', controller]);

    function controller($scope, $localStorage, datacontext, calculator) {
        var vm = this;

        vm.$storage = $localStorage.$default({
            '(Default)': buildEmptyList()
        });

        vm.currentProfile = '(Default)';

        vm.display = {
            label: 'effect',
            showWhite: true,
            showYellow: false,
            showRed: false,
            showPurple: true,
            showGreen: false,
            showBlue: false
        };

        vm.adorns = [];

        vm.classes = [
            new Class('Bruiser', 'Fighter'),
            new Class('Monk', 'Fighter'),
            new Class('Guardian', 'Fighter'),
            new Class('Berserker', 'Fighter'),
            new Class('Paladin', 'Fighter'),
            new Class('Shadowknight', 'Fighter'),

            new Class('Conjuror', 'Mage'),
            new Class('Necromancer', 'Mage'),
            new Class('Illusionist', 'Mage'),
            new Class('Coercer', 'Mage'),
            new Class('Wizard', 'Mage'),
            new Class('Warlock', 'Mage'),

            new Class('Ranger', 'Scout'),
            new Class('Assassin', 'Scout'),
            new Class('Brigand', 'Scout'),
            new Class('Swashbuckler', 'Scout'),
            new Class('Dirge', 'Scout'),
            new Class('Troubador', 'Scout'),
            new Class('Beastlord', 'Scout'),

            new Class('Mystic', 'Priest'),
            new Class('Defiler', 'Priest'),
            new Class('Templar', 'Priest'),
            new Class('Inquisitor', 'Priest'),
            new Class('Warden', 'Priest'),
            new Class('Fury', 'Priest'),
            new Class('Channeler', 'Priest')
        ];
        vm.selectedClass = {};
        $scope.$watch('vm.selectedClass', function() {
            getAdorns();
        });

        vm.minLevel = 96;
        vm.maxLevel = 100;
        $scope.$watch('vm.minLevel', function() {
            getAdorns();
        });
        $scope.$watch('vm.maxLevel', function() {
            getAdorns();
        });

        vm.calculate = calculate;
        vm.reset = reset;
        vm.newProfile = newProfile;
        vm.deleteProfile = deleteProfile;

        datacontext.getStrings().then(function (data) {
            vm.strings = data;
        });

        getAdorns();

        function getAdorns() {
            datacontext.getAdorns(vm.selectedClass, vm.minLevel, vm.maxLevel)
            .then(function (data) {
                vm.adorns = data;
            });
        }

        function calculate() {
            vm.effects = {};
            vm.shoppingList = {};
            vm.materials = {};
            vm.sets = {};

            var adornLists = _.transform( vm.$storage[vm.currentProfile], function ( result, armor ) {
                return result.push( getAdornsFrom(armor) );
            });
            var selectedAdorns = _.flatten( adornLists );

            vm.effects = calculator.getEffects( selectedAdorns );
            vm.shoppingList = calculator.getShoppingList( selectedAdorns );
            vm.materials = calculator.getMaterials(selectedAdorns);
            vm.sets = calculator.getSets(selectedAdorns);
        }

        function reset() {
            vm.effects = {};
            vm.shoppingList = {};
            vm.materials = {};
            vm.sets = {};

            _.forEach(vm.$storage[vm.currentProfile], function(value) {
                resetArmor(value);
            });
        }

        function newProfile(profile) {
            vm.$storage[profile] = buildEmptyList();
            vm.currentProfile = profile;
        }

        function buildEmptyList() {
            return [
                new Armor('Head'),
                new Armor('Shoulders'),
                new Armor('Chest'),
                new Armor('Forearms'),
                new Armor('Hands'),
                new Armor('Legs'),
                new Armor('Feet'),
                new Armor('Primary'),
                new Armor('Secondary'),
                new Armor('Ranged'),
                new Armor('Cloak'),
                new Armor('Charm'),
                new Armor('Charm'),
                new Armor('Neck'),
                new Armor('Ear'),
                new Armor('Ear'),
                new Armor('Finger'),
                new Armor('Finger'),
                new Armor('Wrist'),
                new Armor('Wrist'),
                new Armor('Waist')
            ];
        }

        function deleteProfile() {
            if (vm.currentProfile != '(Default)') {
                delete vm.$storage[vm.currentProfile];
                vm.currentProfile = '(Default)';
            }
        }
    }
})();
