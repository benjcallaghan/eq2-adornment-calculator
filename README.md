# README

# Prerequisites
NodeJS, MongoDB

# First Time
To initialize the database, run the application and go to the endpoint /api/soe.

# Notes
The level filter uses the itemlevel field, which does not match the level seen in game.
Some blue adornments are identical except for which class can equip them. For accurate results, select your class at the top of the page.