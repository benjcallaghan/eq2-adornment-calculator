﻿var _ = require('lodash');
var context = require('./datacontext');
var effectConverter = require('./effectConverter');
var strings = require('./dictionaries/strings.json');

exports.parseSoe = function (itemList) {
    return _.map(itemList, function (item) {
        var adorn = new context.Adornment({
            _id: item.id,
            name: item.displayname,
            color: item.typeinfo.color,
            slots: _.map(item.typeinfo.slot_list, function(slot) {
                return slot.displayname;
            }),
            modifiers: getModifiers(item),
            tier: Math.ceil(item.itemlevel / 10),
            classes: _.map(item.typeinfo.classes, function(c) {
                return c.displayname;
            }),
            level: item.leveltouse,
            sortName: item.displayname,
            willStack: true,
            triggers: {}
    });

        if (item.effect_list) effectConverter.parse(adorn, item.effect_list);
        processDescription(adorn);
        if (adorn.color === 'white') processWhite(adorn);
        if (item.setbonus_info) processSetbonus(adorn, item);

        return adorn;
    });
}

function getModifiers(soeItem) {
    var modifiers = _.transform(soeItem.modifiers, function (result, modifier, key) {
        var resultKey = key;

        // Inconsistency in SOE Data Feeds. Key for modifiers and growth tables are the same except for these five.
        // Key in growth tables is the displayname of the modifiers.
        if (key === 'agility' || key === 'strength' || key === 'stamina' || key === 'intelligence' || key === 'wisdom')
            resultKey = modifier.displayname;

        result[resultKey] = modifier.value;
    });

    if (soeItem.typeinfo.color === 'green') {
        _.forEach(soeItem.growth_table, function(item) {
            _.forEach(item, function (value, key) {
                if (modifiers[key]) modifiers[key] += value;
                else modifiers[key] = value;
            });
        });
    }

    return modifiers ? modifiers : {};
}

function processDescription(adorn) {
    adorn.description = _.map(adorn.modifiers, function (value, key) {
        var desc = '';

        if (value) {
            // Round to one decimal place.
            var roundedValue = Math.round(value * 10) / 10;
            desc = roundedValue;
        }

        if (strings[key]) {
            if (value && !_.contains(strings[key], '%')) desc += ' ';
            desc += strings[key];
        } else {
            desc += key;
        }

        if (value > 0) {
            desc = '+' + desc;
        }

        return desc;
    }).join(',');

    _.forEach(adorn.triggers, function(value, trigger) {
        if (value) {
            adorn.description = strings[trigger] + ' ' + adorn.description;
            
            // Equivalent to break;
            return false;
        }
    });

    if (!adorn.willStack) adorn.description += '*';
}

function processWhite(adorn) {
    if (_.contains(adorn.name, 'Superior')) {
        adorn.description += ' (S)';
        adorn.quality = 2;
    }
    if (_.contains(adorn.name, 'Greater')) {
        adorn.description += ' (G)';
        adorn.quality = 1;
    }
    if (_.contains(adorn.name, 'Lesser')) {
        adorn.description += ' (L)';
        adorn.quality = 0;
    }

    var index = adorn.name.lastIndexOf(' ');
    adorn.sortName = adorn.name.substring(0, index);
}

function processSetbonus(adorn, item) {
    adorn.setbonus = { 'name': item.setbonus_info.displayname, 'list': item.setbonus_list };

    if (adorn.description !== '') {
        adorn.description += ' ';
    }

    adorn.description += 'Set: ' + adorn.setbonus.name;
}