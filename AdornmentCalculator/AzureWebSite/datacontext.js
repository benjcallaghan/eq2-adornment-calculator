﻿var mongoose = require('mongoose');
//var connectionString = process.env.CUSTOMCONNSTR_MONGOLAB_URI || 'mongodb://localhost/adornments';
var connectionString = 'mongodb://MongoLab-b8:oGKGrOgyP5uVfEP9EdNWDbQPyU_kpgYa6zSPYgCkdBg-@ds035747.mongolab.com:35747/MongoLab-b8';
mongoose.connect(connectionString);

var db = mongoose.connection;
db.on( 'error', console.error.bind( console, 'connection error:' ) );
db.once( 'open', function callback() {
    console.log( 'connection successful' );

    var adornSchema = mongoose.Schema( {
        _id: Number,
        name: String,
        color: String,
        slots: [String],
        modifiers: {},
        description: String,
        tier: Number, // Only used in white adorns. Defines the materials to use.
        classes: [String],
        level: Number,
        sortName: String, // Only used in white adorns to allow a different degree of sorting.
        willStack: Boolean,
        triggers: {},
        quality: Number, // Only used in white adorns. 0=Lesser, 1=Greater, 2=Superior
        setbonus: {}
    });

    var adorn = mongoose.model( 'Adorn', adornSchema );
    exports.Adornment = adorn;
});