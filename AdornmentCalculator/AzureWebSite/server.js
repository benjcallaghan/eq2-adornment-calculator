﻿var http = require('http');
var url = require('url');
var _ = require('lodash');
var manager = require('./datamanager');
var strings = require('./dictionaries/strings.json');

var express = require( 'express' );
var app = express();

app.use(express.static(__dirname));

app.get( '/api/adorns', function ( req, res ) {
    var urlParts = url.parse(req.url, true);
    var query = urlParts.query;

    manager.getAdorns( query, function ( err, results ) {
        if (err) console.error(err);
        var list = _.sortBy(results, ['sortName', 'quality']);
        res.json( list );
    });
});

app.get( '/api/soe', function ( req, res ) {
    // 3360 adornments as of 11/01/2014
    for (var i = 0; i < 40; i++) {
        getSoeChunk(i);
    }
    res.sendStatus(204);
});

app.get('/api/strings', function(req, res) {
    res.json(strings);
});

function getSoeChunk(skip) {
    var url = 'http://census.soe.com/s:benjadorncalculator/json/get/eq2/item/?typeinfo.name=adornment&c:limit=100&c:start=' + (skip * 100);
    http.get( url, function ( httpRes ) {
        var buffer = '';

        httpRes.on( "data", function ( chunk ) {
            buffer += chunk;
        });

        httpRes.once( "end", function () {
            var soeData = JSON.parse( buffer );
            manager.saveAdorns(soeData.item_list);
            console.log('Chunk #' + skip + ' complete');
        });
    });
}

app.listen( process.env.PORT || 8080 );