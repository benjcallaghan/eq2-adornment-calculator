﻿var _ = require('lodash');
var triggers = require('./dictionaries/triggers.json');
var effects = require('./dictionaries/effects.json');
var procs = require('./dictionaries/procs.json');
var percentages = require('./dictionaries/percentages.json');
var groups = require('./dictionaries/group.json');
var grouppct = require('./dictionaries/grouppct.json');

var adorn = {};

exports.parse = function (item, effectList) {
    adorn = item;

    _.forEach(effectList, function (effect) {
        // Short circuit if the adornment is modified.
        // Each effect will only have one modification.
        checkStack(effect) ||
            checkTriggers(effect) ||
            checkProcs(effect) ||
            checkEffects(effect);
    });

    simplifyTriggers();
}

function checkStack(effect) {
    if (_.contains(effect.description, 'not stack')) {
        adorn.willStack = false;
    }
}

function checkTriggers(effect) {
    var modified = false;

    // Triggers are stored 'backwards' in JSON.
    // The key is human-readable; the value is the internal property name.
    _.forEach(triggers, function (triggerKey, trigger) {
        if (_.contains(effect.description, trigger)) {
            adorn.triggers[triggerKey] = true;

            // Equivalent to break;
            modified = true;
            return false;
        }
    });

    return modified;
}

function checkEffects(effect) {
    var modified = false;

    // Need to distinguish between four types of effects.
    // Ex: Group Max Health 20%, Group Max Health 20000
    // Ex: Max Health 20%, Max Health 20000
    var group = _.contains(effect.description, 'group');
    var pct = _.contains(effect.description, '%');

    var dictionary;
    if (group && pct) dictionary = grouppct;
    else if (group) dictionary = groups;
    else if (pct) dictionary = percentages;
    else dictionary = effects;

    // Effects are stored 'backwards' in JSON.
    // The key is human-readable; the value is the internal property name.
    _.forEach(dictionary, function (effectKey, effectDescription) {
        if (_.contains(effect.description, effectDescription)) {
            // Match all numbers and remove commas.
            var value = effect.description.match(/\d[0-9,]*(\.\d)?/)[0].replace(',', '');

            var reduce = _.contains(effect.description, "Reduce");
            adorn.modifiers[effectKey] = reduce ? -parseFloat(value): parseFloat(value);

            modified = true;
        }
    });

    return modified;
}

function checkProcs(effect) {
    var modified = false;

    // Procs are stored 'backwards' in JSON.
    // The key is human-readable; the value is the internal property name.
    _.forEach(procs, function (procKey, proc) {
        if (_.contains(effect.description, proc)) {
            // Declare the property without giving it a meaningful value.
            // Procs vary for each character, so we avoid a specific value.
            adorn.modifiers[procKey] = 0;

            // Equivalent to break;
            modified = true;
            return false;
        }
    });

    return modified;
}

function simplifyTriggers() {
    // Simplify triggers. Some adornments list the same effect under multiple triggers.
    if (adorn.triggers.onHeal && adorn.triggers.onHit) {
        adorn.triggers.onHeal = false;
        adorn.triggers.onHit = false;
        adorn.triggers.onCast = true;
    }
    // If any two, or all three.
    else if ((adorn.triggers.onCritHit && adorn.triggers.onCritSpell) ||
            (adorn.triggers.onCritHit && adorn.triggers.onCritHeal) ||
            (adorn.triggers.onCritHeal && adorn.triggers.onCritSpell)) {
        adorn.triggers.onCritHit = false;
        adorn.triggers.onCritSpell = false;
        adorn.triggers.onCritHeal = false;
        adorn.triggers.onCrit = true;
    }
    else if (adorn.triggers.onSpell && adorn.triggers.onHit) {
        adorn.triggers.onSpell = false;
    }
}