﻿(function () {
    'use strict';

    angular.module('app').directive('adornSelect', [adornSelect]);

    function adornSelect() {
        var directive = {
            link: link,
            restrict: 'E',
            templateUrl: 'app/directives/adorn-select.html',
            scope: {
                adorn: '=',
                list: '=',
                display: '=',
                color: '=',
                slot: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }
})();