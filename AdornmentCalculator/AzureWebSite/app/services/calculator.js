﻿(function () {
    'use strict';
    
    var serviceId = 'calculator';
    
    angular.module('app').factory(serviceId, ['datacontext', calculator]);
    
    function calculator(datacontext) {
        var vm = this;
        datacontext.getStrings().then(function (data) {
            vm.strings = data;
        });
        
        var service = {
            getShoppingList: getShoppingList,
            getEffects: getEffects,
            getMaterials: getMaterials,
            getSets: getSets
        };
        
        return service;
        
        function getShoppingList(list) {
            var adornGroups = _.groupBy(list, 'name');
            return _.transform(adornGroups, function (adornGroup, adorns, adornKey) {
                adornGroup[adornKey] = adorns.length;
            });
        }
        
        function getEffects(list) {
            var modifiers = _(list).flatten('modifiers').cloneDeep();
            
            var combined = _.reduce(modifiers, function (sum, modifier) {
                for (var name in modifier) {
                    if (sum[name]) {
                        sum[name] += modifier[name];
                    } else {
                        sum[name] = modifier[name];
                    }
                }
                return sum;
            }, {});
            
            var effects = {};
            for (var prop in combined) {
                effects[prop] = getDescription(prop, combined[prop]);
            }
            return effects;
        }
        
        function getDescription(key, value) {
            var desc = '';
            
            if (value) {
                // Round to one decimal place.
                var roundedValue = Math.round(value * 10) / 10;
                desc = roundedValue;
            }
            
            if (vm.strings[key]) {
                if (value && !_.contains(vm.strings[key], '%')) desc += ' ';
                desc += vm.strings[key];
            } else {
                desc += key;
            }
            
            if (value > 0) {
                desc = '+' + desc;
            }
            
            return desc;
        }
        
        function getMaterials(list) {
            var whites = _.where(list, { color: 'white' });
            var groups = _.groupBy(whites, 'tier');
            
            var materials = {};
            _.forEach(groups, function (group, tier) {
                materials[tier] = getMaterialsForTier(group);
            });
            
            return materials;
        }
        
        function getMaterialsForTier(tierGroup) {
            var superiors = _.filter(tierGroup, function (adorn) {
                return _.contains(adorn.name, 'Superior');
            });
            var greaters = _.filter(tierGroup, function (adorn) {
                return _.contains(adorn.name, 'Greater');
            });
            var lessers = _.filter(tierGroup, function (adorn) {
                return _.contains(adorn.name, 'Lesser');
            });
            
            var sGroups = _.groupBy(superiors, 'name');
            var gGroups = _.groupBy(greaters, 'name');
            var lGroups = _.groupBy(lessers, 'name');
            
            var sCombines = 0;
            var gCombines = 0;
            var lCombines = 0;
            
            _.forEach(sGroups, function (group) {
                sCombines += group.length;
            });
            _.forEach(gGroups, function (group) {
                gCombines += Math.ceil(group.length / 3);
            });
            _.forEach(lGroups, function (group) {
                lCombines += Math.ceil(group.length / 10);
            });
            
            var materials = {
                mana: 0,
                infusion: 0,
                powder: 0,
                fragment: 0
            };
            
            if (sCombines) {
                materials.mana += sCombines;
                // Net cost plus the initial material (returned as byproduct).
                materials.infusion += sCombines + 1;
                materials.powder += sCombines * 3;
                materials.fragment += sCombines * 6;
            }
            if (gCombines) {
                materials.infusion += gCombines;
                // Net cost plus the initial material (returned as byproduct).
                materials.powder += gCombines + 1;
                materials.fragment += gCombines * 6;
            }
            if (lCombines) {
                materials.powder += lCombines;
                // Net cost plus the initial material (returned as byproduct).
                materials.fragment += lCombines * 3 + 3;
            }
            
            if (sCombines && gCombines) {
                // One infusion leftover from superiors that can be used on a greater.
                materials.infusion -= 1;
            }
            if (gCombines && lCombines) {
                // One powder leftover from greaters that can be used on a lesser.
                materials.powder -= 1;
            }
            
            return materials;
        }
        
        function getSets(list) {
            // Only look at adornments that are part of a set.
            var adorns = _.filter(list, function (adorn) {
                return adorn.setbonus;
            });
            
            // Grab the sets from the adornments and group by name.
            var adornmentSets = _.map(adorns, 'setbonus');
            var groups = _.groupBy(adornmentSets, 'name');

            // Each group is a collection of identical sets.
            var setbonuses = _.map(groups, function (sets, setName) {
                var setbonus = {};
                setbonus.name = setName;
                
                // The number of adornments selected in that set is equal to the group's length.
                setbonus.equipped = sets.length; 
                
                // The max number of adornments in a set is equal to the length of the set.
                var currentSet = sets[0].list;
                setbonus.max = currentSet.length;

                setbonus.effects = {};
                var temp = {};

                // Stop reading after we reach the number we have equipped.
                for (var i = 0; i < setbonus.equipped; i++) {
                    var currentBonus = currentSet[i];
                    
                    // Look at each effect in the current bonus (ignoring the requireditems property).
                    for (var name in currentBonus) {
                        if (name != 'requireditems') {
                            // If the effect already exists, add to it. Else, create it.
                            if (temp[name]) temp[name] += currentBonus[name];
                            else temp[name] = currentBonus[name];
                        }
                    }
                }
                
                // Replace the internal modifier codes with a readable description.
                for (var prop in temp) {
                    setbonus.effects[prop] = getDescription(prop, temp[prop]);
                }

                return setbonus;
            });

            return setbonuses;
        }
    }
})();