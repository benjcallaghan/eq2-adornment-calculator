﻿var _ = require( 'lodash' );
var context = require( './datacontext' );
var converter = require('./soeConverter');

exports.saveAdorns = function ( adornList ) {
    var adorns = converter.parseSoe( adornList );
    _.forEach( adorns, function ( adorn ) {
        addOrUpdate( adorn );
    });
}

function addOrUpdate( adorn ) {
    var upsertData = adorn.toObject();
    delete upsertData._id;

    context.Adornment.update( { _id: adorn._id }, upsertData, { upsert: true }, function ( err ) {
        if ( err ) throw err;
    });
}

exports.getAdorns = function (query, callback) {
    var conditions = {
        level: {}
    };
    if (query.class) conditions.classes = query.class;
    if (query.min) conditions.level.$gte = query.min;
    if (query.max) conditions.level.$lte = query.max;
    
    context.Adornment.find(conditions, callback);
}