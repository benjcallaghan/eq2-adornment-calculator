﻿function Armor(slot) {
    var vm = this;
    vm.slot = slot;
    resetArmor(vm);
};

function getAdornsFrom(armor) {
    return _.filter(armor, function (value, key) {
        var isAdorn = key !== 'slot' && key !== 'getAdorns' && key !== '$$hashKey' && key !== 'reset';
        return isAdorn && value;
    });
}

function resetArmor(armor) {
    // Define available properties for binding.
    armor.white1 = null;
    armor.white2 = null;
    armor.white3 = null;
    armor.yellow = null;
    armor.yellow2 = null;
    armor.red = null;
    armor.red2 = null;
    armor.purple = null;
    armor.purple2 = null;
    armor.green = null;
    armor.blue = null;
    armor.blue2 = null;
}

function Class(name, type) {
    var vm = this;
    vm.name = name;
    vm.type = type;
}