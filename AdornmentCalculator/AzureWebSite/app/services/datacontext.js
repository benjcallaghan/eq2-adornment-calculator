﻿(function () {
    'use strict';

    var serviceId = 'datacontext';

    angular.module('app').factory(serviceId, ['$http', datacontext]);

    function datacontext($http) {
        var service = {
            getAdorns: getAdorns,
            getStrings: getStrings
        };

        return service;

        function getAdorns(c, min, max) {
            var query = [];
            if (c && c.name) query.push('class=' + c.name);
            if (min) query.push('min=' + min);
            if (max) query.push('max=' + max);

            var response = $http.get('api/adorns?' + query.join('&'))
            .then(function (httpResponse) {
                return httpResponse.data;
            });
            return response;
        }

        function getStrings() {
            var response = $http.get('api/strings').then(function(httpResponse) {
                return httpResponse.data;
            });
            return response;
        }
    }
})();