﻿(function () {
    'use strict';

    angular.module('app').directive('armorRow', ['$filter', armorRow]);

    function armorRow($filter) {
        var directive = {
            link: link,
            restrict: 'A',
            templateUrl: 'app/directives/armor-row.html',
            scope: {
                'armor': '=',
                'adornList': '=',
                'display': '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            scope.showWhite = false;
            scope.showYellow = false;
            scope.showRed = false;
            scope.showPurple = false;
            scope.showGreen = false;
            scope.showBlue = false;

            createLists(scope);

            scope.$watch('adornList', function(newValue, oldValue) {
                createLists(scope);
            });
        }

        function createLists(scope) {
            scope.whiteList = $filter('adornFilter')(scope.adornList, 'white', scope.armor.slot);
            scope.yellowList = $filter('adornFilter')(scope.adornList, 'yellow', scope.armor.slot);
            scope.redList = $filter('adornFilter')(scope.adornList, 'red', scope.armor.slot);
            scope.purpleList = $filter('adornFilter')(scope.adornList, 'purple', scope.armor.slot);
            scope.greenList = $filter('adornFilter')(scope.adornList, 'green', scope.armor.slot);
            scope.blueList = $filter('adornFilter')(scope.adornList, 'blue', scope.armor.slot);

            if (scope.whiteList.length > 0)
                scope.showWhite = true;
            if (scope.yellowList.length > 0)
                scope.showYellow = true;
            if (scope.redList.length > 0)
                scope.showRed = true;
            if (scope.purpleList.length > 0)
                scope.showPurple = true;
            if (scope.greenList.length > 0)
                scope.showGreen = true;
            if (scope.blueList.length > 0)
                scope.showBlue = true;
        }
    }

})();