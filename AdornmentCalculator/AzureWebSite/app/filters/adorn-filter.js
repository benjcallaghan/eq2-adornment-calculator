﻿(function () {
    'use strict';

    var id = 'adornFilter';

    angular.module('app').filter(id, [adornFilter]);

    function adornFilter() {
        return filter;
    }

    function filter(list, color, slot) {
        var adorns = _.where(list, { color: color, slots: [slot] });
        return adorns;
    }
})();