﻿(function () {
    'use strict';

    var id = 'app';

    // TODO: Inject modules as needed.
    var app = angular.module(id, [
        'ngStorage'
    ]);

    app.run(['$q', '$rootScope',
        function ($q, $rootScope) {

        }]);
})();